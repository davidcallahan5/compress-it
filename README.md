# Compress It
Compress It is a compression service project. This project aims to test C++11 and above with boost to connect a server with a client that send handles compressed data.

## Target Platform
OS: Red Hat Enterprise Linux 8.2 (Oracle VM VirtualBox image on Windows 10)
Language: C++14 compiled using GCC 8.3.1

## Inner Workings of Code
main
 * Initializes project.
 * Loops on accepting TCP connections
 * Passes received data from TCPServer to RequestCodeHandler

TCPServer
 * Accepts connections.
 * Reads, and writes data.

RequestCodeHandler
 * Receives data from main.
 * Compresses string.

## Third Party Libraries
The only third party library used was boost_system. Boost was used mostly for handling the TCP connection.

## Project Assumptions
 * The TCP client would not be a part of this application/submission. I tested this application using an external TCPClient using boost to send and receive messages
 * That only one client would be connecting at a time. The connection was made to be synchronous and any other clients attempting to connect would not be able to since the application would be refused. This could also be a future improvement.
 * That the appication will only be ran locally.
 * That there should be no use of C specific functions used throughout the project. This posed as a challenge since the boost TCP connection way was something new to be learned and used a considerable amount of time.  

## Future Improvements
 * This project only has certain parts of the application working. Improvements made to the actual connection and handling of packet headers and data would take the priority.
 * Getting packet data and connecting with RequestHandler. Existing code would make this simple.
 * Using cmake for a better project structure. I used only a makefile out of convenience since it was faster to make.
 * Build project without entire boost includes. Logged bugfix to not build with all of boost includes. 
 * Test that application will properly handle TCP requests and remain connected or searching for connections.
 * Data for TCPServer read and write should both use streambufs using the specified packet notation.
 * Ensure RequestCodeHandler may compress given data, ping, get status and reset status.
