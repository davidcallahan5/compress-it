#include <iostream>
#include "TCPServer.hpp"

using namespace compress_it;

TCPServer::TCPServer():
  m_local_address("127.0.0.1"),
  m_port(4000),
  m_mtx{},
  m_io{},
  m_socket(m_io),
  m_acceptor(m_io, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), m_port))
{

}

void TCPServer::accept()
{
  std::lock_guard<std::mutex> lock(m_mtx);
  m_acceptor.accept(m_socket);
}

boost::asio::streambuf& TCPServer::readData()
{
  boost::asio::streambuf buf;
  
  //Read 8 bytes (packet header)
  std::size_t n = boost::asio::read(m_socket, buf, boost::asio::transfer_at_least(8));

  std::cout << n << std::endl;

  //Get number of bytes for packet data. Read amount of given size
  //boost::asio::read(m_socket, buf, boost::asio::transfer_at_least(0));
  return buf;
}

void TCPServer::write(Packet pack)
{
  std::lock_guard<std::mutex> lock(m_mtx);
  boost::system::error_code ignored_error;
  boost::asio::write(m_socket, boost::asio::buffer("test"), ignored_error);
}