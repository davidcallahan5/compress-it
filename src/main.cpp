#include <iostream>
#include "TCPServer.hpp"
#include "RequestCodeHandler.hpp"

using namespace compress_it;

int main()
{
  std::cout << "Server for 'Compress It' service has initialized" << std::endl;

  TCPServer tcp;
  RequestCodeHandler requestCodeHandler;
  Packet pack;
  pack.magic_id = 54355345;
  pack.payload_length = 0;
  pack.request_code = 4;
  
  //Loop indefinitely to handle multiple requests
  try
  {
    while(1)
    {      
      std::cout << "Waiting for client request..." << std::endl;

      //Wait and listen for client to connect
      tcp.accept();

      //read incoming data from client
      boost::asio::streambuf& buf(tcp.readData());
      
      std::istream is(&buf);  
      std::string line;

      //handle the pack sent in
      requestCodeHandler.handleCode(pack);

      //Respond to pack with our modified pack
      tcp.write(pack);

      std::cout << "message sent." << std::endl;
    }    
  }
  catch(const std::exception& e)
  {
    std::cerr << e.what() << std::endl;
  }
  catch(...)
  {
    std::cerr << "Error outside std::exception" << std::endl;
  }

  return 0;
}
