#include <iostream>
#include "RequestCodeHandler.hpp"

using namespace compress_it;

RequestCodeHandler::RequestCodeHandler():
  m_mtx{},
  m_total_bytes_received(0),
  m_total_bytes_sent(0)
{

}

void RequestCodeHandler::handleCode(Packet& packet)
{
  std::lock_guard<std::mutex> lock(m_mtx);

  //update bytes received counted (payload and header
  //@TODO get sizes for payload and header.
  if(m_total_bytes_received + packet.payload_length < UINT32_MAX)
  {
    m_total_bytes_received += packet.payload_length;
  }
  else
  {
    m_total_bytes_received = UINT32_MAX;
  }

  //Handle request code
  switch(static_cast<RequestCode>(packet.request_code))
  {
    case RequestCode::ping:
    {
      packet.payload_length = 0;
      packet.request_code = static_cast<std::underlying_type_t<StatusCode>>(StatusCode::okay);
    }
    break;

    case RequestCode::get_stats:
    {

    }
    break;

    case RequestCode::reset_stats:
    {

    }
    break;

    case RequestCode::compress:
    {
      std::string compressed_str = compress("aaaabbcccc");
    }
    break;

    default:
    {
      std::cout << "RequestCode: " << (int)packet.request_code << " is not supported." << std::endl;
      packet.request_code = static_cast<std::underlying_type_t<StatusCode>>(StatusCode::unsupported_rc_type);
    }
    break;
  }

  //update bytes sent counted (payload length and headers)
  if(m_total_bytes_sent + packet.payload_length < UINT32_MAX)
  {
    m_total_bytes_sent += packet.payload_length;
  }
  else
  {
    m_total_bytes_sent = UINT32_MAX;
  } 
}

std::string RequestCodeHandler::compress(const std::string& str)
{
  std::string temp = "";
  std::string result = "";

  //Loop through each character saving each char into a temp string. O(n)
  for(auto it = str.cbegin(); it != str.cend(); it++)
  {
    if(temp.empty() || temp[0] == *it)
    {
      //If the temp string is empty then there is no need to compare anything
      //If duplicate, append char to temp
      temp += *it;
    }
    else
    {
      //When a non-duplicate character is found check if count is 3 or higher.
      if(temp.length() > 2)
      {
        //if count is 3 or higher then compress string , then append new string to result
        result += std::to_string(temp.length());
        result += temp[0];
      }
      else
      {
        //otherwise append string as is to running string
        result += temp;
      }
 
      //Reset temp. include char used for earlier comparison
      temp.clear();
      temp += *it;
    }
  }
  
  //Check if there are any characters left in temp to append to result
  if(temp.length() > 2)
  {
    result += std::to_string(temp.length());
    result += temp[0];
  }
  else
  {
    result += temp;
  }
  
  std::cout << "result: " << result << ", input string: " << str << std::endl;

  return result;
}