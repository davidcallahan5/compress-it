/*
  Author: David Callahan
  Date: Jul 24th 18:15 2020
*/
#ifndef __TCPSERVER_HPP
#define __TCPSERVER_HPP

#include <mutex>
#include <boost/asio.hpp>

namespace compress_it
{
  /**
   * Packet header length
   */
  const uint8_t header_length = 8;

  /**
   * Packet header contents
   */
  struct Packet
  {
    uint32_t magic_id;
    uint16_t payload_length;
    uint16_t request_code;
  };

  /**
    A TCP server that receives and sends data to a TCP client.
    * listens on port 4000
    * sets a 32 bit magic value to 0x53545259
    * payload length of 16 bits
    * request/status code of 16 bits
    * sent in network byte order (big-endian)
   */
  class TCPServer
  {
  public:
    TCPServer();
    ~TCPServer() {};

    /**
     * accepts connected TCP client 
     */
    void accept();

    /**
     * reads packet from client
     * @return input stream
     */
    boost::asio::streambuf& readData();

    /**
     * writes packet back to client 
     */
    void write(Packet pack);

  private:
    std::string m_local_address;
    uint16_t m_port;
    std::mutex m_mtx;

    boost::asio::io_service m_io;
    boost::asio::ip::tcp::socket m_socket;
    boost::asio::ip::tcp::acceptor m_acceptor;
  };
}

#endif