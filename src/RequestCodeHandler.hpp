#ifndef __REQUESTCODEHANDLER_HPP
#define __REQUESTCODEHANDLER_HPP

#include "TCPServer.hpp"

namespace compress_it
{
  /**
   * Request codes handled for incoming messages
   */
  enum class RequestCode
  {
    ping = 1,
    get_stats = 2,
    reset_stats = 3,
    compress = 4
  };

  /**
   * Status codes handled for outgoing messages
   */
  enum class StatusCode
  {
    okay = 0,
    unknown = 1,
    too_large = 2,
    unsupported_rc_type = 3
    //4-32 reserved
    //33-128 implementer defined
  };

  class RequestCodeHandler
  {
  public:
    RequestCodeHandler();
    ~RequestCodeHandler() {};

    /**
     * Handles incoming packets by passing data via Request Code
     * @param packet The incoming packet data to be handled.
     */
    void handleCode(Packet& packet);

  private:
    /**
     * Request Code 4
     * Compresses string to a smaller length
     * aa => aa
     * aaa => 3a 
     * @param str The string to be compressed
     * @return The compressed string
     */
    std::string compress(const std::string& str);

  private:
    std::mutex m_mtx;
    uint32_t m_total_bytes_received;
    uint32_t m_total_bytes_sent;

  };
}

#endif