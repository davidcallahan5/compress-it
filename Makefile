CXX := g++
CXXFLAGS := -std=c++14 -Wall -g -pthread -D_GLIBCXX_USE_CXX11_ABI=1
BUILD := build
BIN := bin
OBJ_DIR := $(BUILD)/obj
APP_DIR := $(BUILD)/app
TARGET := server

BOOST_ROOT := /usr/include/boost

INCLUDES := $(BOOST_ROOT) 
LIBS := boost_system

SRC := $(wildcard src/*.cpp)
OBJECTS := $(SRC:%.cpp=$(OBJ_DIR)/%.o)

#build project
all: build $(APP_DIR)/$(TARGET)

$(OBJ_DIR)/%.o: %.cpp
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) $(INCLUDE) -c $< -o $@ $(LDFLAGS)

$(APP_DIR)/$(TARGET): $(OBJECTS)
	@mkdir -p $(@D)
	$(CXX) $(CXXFLAGS) -I${INCLUDES} -o $(APP_DIR)/$(TARGET) $^ $(LDFLAGS) -l ${LIBS}

.PHONY: all build clean debug release

build:
	@mkdir -p $(APP_DIR)
	@mkdir -p $(OBJ_DIR)
